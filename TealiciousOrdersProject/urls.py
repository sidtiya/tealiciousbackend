"""TealiciousOrdersProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from rest_framework.authtoken import views as rest_framework_views

from OnlineOrderApp import views, apis

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^AboutUS.html/$', TemplateView.as_view(template_name='AboutUS.html')),

    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('OnlineOrderApp.urls')),
    # ShareTea Owner Related URLS
    url(r'^sharetea_owner/sign-in/$', auth_views.login,
    {'template_name': 'sharetea_owner/sign_in.html'},
    name = 'restaurant-sign-in'),
    url(r'^sharetea_owner/sign-out/$', auth_views.logout,
    {'next_page': 'restaurant-sign-in'},
    name = 'restaurant-sign-out'),
    url(r'^api/', include('OnlineOrderApp.urls')),
    url(r'^sharetea_owner/$', views.owner_home,name='owner-home'),
    url(r'^sharetea_owner/sign-up', views.owner_sign_up,name='owner-sign-up'),

    url(r'^sharetea_owner/account/$', views.owner_account,name='owner-acct'),
    url(r'^sharetea_owner/drink/$', views.owner_drink,name='owner-drink'),
    url(r'^sharetea_owner/drink/add/$', views.owner_add_drink,name='owner-add-drink'),
    url(r'^sharetea_owner/drink/edit/(?P<drink_id>\d+)/$', views.owner_edit_drink, name = 'owner-edit-drink'),
    url(r'^sharetea_owner/order/$', views.owner_order,name='owner-order'),
    url(r'^sharetea_owner/report/$', views.owner_report,name='owner-report'),
    url(r'^sharetea_owner/ingredient/$', views.owner_ingredient,name='owner-ingredient'),
    url(r'^sharetea_owner/ingredient/add/$', views.owner_add_ingredient,name='owner-add-ingredient'),
    url(r'^sharetea_owner/ingredient/edit/(?P<ingredient_id>\d+)/$', views.owner_edit_ingredient, name = 'owner-edit-ingredient'),


    # User_Client iOS App related URLS
    url(r'^api/social/', include('rest_framework_social_oauth2.urls')),
    url(r'^api/sharetea_owner/order/notification/(?P<last_request_time>.+)/$', apis.owner_order_notification),
    url(r'^api/sharetea_owner/owners/all/$', apis.get_all_owners, name = 'get-all-owners'),
    url(r'^api/sharetea_owner/owner/(?P<owner_id>\d+)/$', apis.get_owner_by_id, name = 'owner-get-by-id'),
    url(r'^api/sharetea_owner/orders/all/$', apis.get_all_orders, name = 'get-all-orders'),
    url(r'^api/sharetea_owner/order/(?P<order_id>\d+)/$', apis.get_order_by_id, name = 'get-order-by-id'),
    url(r'^api/sharetea_owner/ready_orders/all/$', apis.get_all_ready_orders, name = 'get-all-ready-orders'),


    # API for Customer
    url(r'^api/customer/owners/all/$', apis.get_all_owners, name = 'customer-get-all-owners'),
    # url(r'^api/customer/drinks/all/$', apis.get_all_products, name = 'customer-get-all-products'),
    #url(r'^api/customer/drinks/all/(?P<owner_id>\d+)/$', apis.customer_get_drinks, name = 'customer-get-drinks'),
    # url(r'^api/customer/drinks/all/$', apis.customer_get_drinks, name = 'customer-get-drinks'),
    url(r'^api/customer/order/add/$', apis.customer_add_order, name = 'customer-add-order'),
    url(r'^api/customer/order/latest/$', apis.customer_get_latest_order, name = 'customer-get-latest-order'),
    url(r'^api/customer/access_token/(?P<customer_id>\d+)/$', apis.customer_get_access_token, name = 'customer-get-access-token'),
    url(r'^api/get_auth_token/$', rest_framework_views.obtain_auth_token, name='get_auth_token'),
    url(r'^api/customer/drinks/all/$', apis.customer_get_drinks_by_category, name = 'customer-get-drinks-by-category'),
    url(r'^api/customer/ingredients/all/$', apis.customer_get_topping, name='customer-get-ingredients'),
    url(r'^api/customer/categories/all/$', apis.customer_get_categories, name = 'customer-get-categories'),
    url(r'^api/customer/drinks/total/$', apis.customer_get_total_drinks, name = 'customer-get-total-drinks'),
    url(r'^api/customer/drinks/favorites/$', apis.customer_get_favorite_drinks, name = 'customer-get-favorite-drinks'),

    # APIs for DRIVERS
    url(r'^api/driver/orders/ready/$', apis.driver_get_ready_orders),
    url(r'^api/driver/order/pick/$', apis.driver_pick_order),
    url(r'^api/driver/order/latest/$', apis.driver_get_latest_order),
    url(r'^api/driver/order/complete/$', apis.driver_complete_order),
    url(r'^api/driver/revenue/$', apis.driver_get_revenue),
    url(r'^__debug__/', include(debug_toolbar.urls)),

    url(r'^api/cart/add/$', apis.add_to_cart),
    url(r'^api/cart/remove/$', apis.remove_from_cart),
    url(r'^api/cart/$', apis.get_cart),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
