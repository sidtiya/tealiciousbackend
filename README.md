# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Clone Repo https://sidtiya@bitbucket.org/sidtiya/tealiciousbackend.git
* Go to Cloned Directory

### Run Server Locally ###
* pip install -r requirements.txt
* python manage.py runserver 0.0.0.0:8070
* go to http://127.0.0.:8070 in your browser

### Clean Database
* https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html

### BackEnd API Documentation ###
* https://documenter.getpostman.com/view/920698/RVnZfd1R

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
* Best Practice Guidelines:
* [Django-Best-Practice-Doc](http://django-best-practices.readthedocs.io/en/latest/applications.html)
* [Django-Best-Practice-VirtualEnv](https://medium.freecodecamp.org/improve-your-django-project-with-these-best-practices-47fd60a7bff3)


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact