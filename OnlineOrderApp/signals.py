import django.dispatch

order_placed = django.dispatch.Signal(
    providing_args=["drink", "customer", "order", "order_total", "quantity"])
