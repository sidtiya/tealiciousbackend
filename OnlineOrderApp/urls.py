from django.conf.urls import url

from . import views, apis

urlpatterns = [
    url(r'^home', views.home),
    url(r'^process_payment/$', apis.payment,name='process-payment')
]
