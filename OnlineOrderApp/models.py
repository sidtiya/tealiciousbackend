from __future__ import unicode_literals

# Django Project Imports
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Project Specific Imports
from  OnlineOrderApp.helpermethods import *
from OnlineOrderApp.Helpers import *

from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings

# This code is triggered whenever a new user has been created and saved to the database

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)



# Create your models here.
class Owner(models.Model):
    """Class of ShareTea Owners"""
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='owner')
    name = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    address = models.CharField(max_length=500)
    zipcode = models.IntegerField(default=98004)
    logo = models.ImageField(upload_to='owner_logo/', blank=False)

    def __str__(self):
        return self.user.username
    """Get User Name"""
    def getName(self):
        return self.name
    """Get User Phone"""
    def getPhone(self):
        return self.phone
    """Get User Instance"""
    def getUser(self):
        return self.user
    """Get User Address"""
    def getAddress(self):
        return self.address


class Customer(models.Model):
    """Class for ShareTea customers"""
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='customer')
    avatar = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    address = models.CharField(max_length=500)

    def __str__(self):
        return self.user.username

    """Get User Name"""
    def getEmail(self):
        return self.user.email

    """Get User Name"""
    def getName(self):
        return self.user.get_full_name()
    """Get User Phone"""
    def getPhone(self):
        return self.phone
    """Get User Instance"""
    def getUser(self):
        return self.user
    """Get User Address"""
    def getAddress(self):
        return self.address
    """Get User Avatar"""
    def getAvatar(self):
        return self.avatar


class Driver(models.Model):
    """Class for ShareTea Drivers"""
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='driver')
    avatar = models.CharField(max_length=500)
    phone = models.CharField(max_length=500)
    address = models.CharField(max_length=500)

    def __str__(self):
        return self.user.get_full_name()


class Category(models.Model):
    name = models.CharField(max_length=500, default='MilkTea')
    # owner = models.ForeignKey(Owner, related_name='owner')
    calories = models.IntegerField(max_length=500, default=0)
    short_description = models.CharField(max_length=500, default='Tea')
    image = models.ImageField(upload_to='category_images/', blank=True, null=True, default = '')
    def __str__(self):
        return self.name


class Drink(models.Model):
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE, related_name='category_owner')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='category')
    name = models.CharField(max_length=500)
    # category = models.CharField(max_length=500 , default='MilkTea')
    short_description = models.CharField(max_length=500)
    image = models.ImageField(upload_to='drink_images/', blank=False)
    price = models.FloatField(default=3.75)
    calories = models.IntegerField(max_length=500, default=0)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.name

# class Favorites(models.Model):


class Ingredient(models.Model):
#    drink = models.ForeignKey(Drink, on_delete=models.CASCADE, default=1)
    name = models.CharField(max_length=500, default='BlackBoba')
    short_description = models.CharField(max_length=500)
    ingredient_type = models.CharField(max_length=1,default='I')
    image = models.ImageField(upload_to='ingredient_images/', blank=False)
    price = models.FloatField(default=0.50)
    calories = models.IntegerField(max_length=500, default=0)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Order(models.Model):
    RECEIVED = 1
    PROCESSING = 2
    MAKING = 3
    READY = 4
    ONTHEWAY = 5
    DELIVERED = 6

    STATUS_CHOICES = (
        (RECEIVED, "Received"),
        (PROCESSING, "Processing"),
        (MAKING, "Making"),
        (READY, "Ready"),
        (ONTHEWAY, "OnTheWay"),
        (DELIVERED, "Delivered"),
    )

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE)
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE, null=True)
    address = models.CharField(max_length=500)
    total = models.FloatField(default=0.000000)
    status = models.IntegerField(choices = STATUS_CHOICES)
    created_at = models.DateTimeField(default = timezone.now)
    picked_At = models.DateTimeField(blank = True, null = True)
    quantity = models.IntegerField(default=0)

    def __str__(self):
        return str(self.id)


class OrderDetails(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_details')
    drink = models.ForeignKey(Drink, on_delete=models.CASCADE)
    ingredients = models.ForeignKey(Ingredient, on_delete=models.CASCADE, related_name='ingredients', default=1)
    quantity = models.IntegerField()
    sub_total = models.FloatField(default=0.000000)
    ingredient = models.ManyToManyField(Ingredient)

    def __str__(self):
        return str(self.id)


from decimal import Decimal


class DrinkRecord(models.Model):
    """
    A record of a how popular a product is.
    This used be auto-merchandising to display the most popular
    products.
    """

    drink = models.OneToOneField(
        'OnlineOrderApp.Drink', verbose_name=("Drink"),
        related_name='stats', on_delete=models.CASCADE)

    # Data used for generating a score
    num_views = models.PositiveIntegerField(('Views'), default=0)
    num_cart_additions = models.PositiveIntegerField(
        ('Cart Additions'), default=0)
    num_purchases = models.PositiveIntegerField(
        ('Purchases'), default=0, db_index=True)

    # Product score - used within search
    score = models.FloatField(('Score'), default=0.00)

    # class Meta:
    #     abstract = True
    #     app_label = 'OnlineOrderApp'
    #     ordering = ['-num_purchases']
    #     verbose_name = _('Drink record')
    #     verbose_name_plural = _('Drink records')

    def __str__(self):
        return _("Record for '%s'") % self.product

class DrinksRecordForUser(models.Model):
    """
    A record of a user's drink record.
    """
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, default=0)
    drink = models.ForeignKey(Drink, on_delete=models.CASCADE, default=0)
    num_orders = models.PositiveIntegerField(
        ('Orders'), default=0, db_index=True)
    def __str__(self):
        return str(self.id)


class UserRecord(models.Model):
    """
    A record of a user's activity.
    """

    # user = models.OneToOneField(User, verbose_name=("User"),
    #                             on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, default=0, db_index=True)

    # Order stats
    num_orders = models.PositiveIntegerField(
        ('Orders'), default=0, db_index=True)
    total_spent = models.DecimalField(('Total Spent'), decimal_places=2,
                                      max_digits=12, default=Decimal('0.00'))
    date_last_order = models.DateTimeField(
        ('Last Order Date'), blank=True, null=True)

    drink = models.ForeignKey(Drink, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.customer.user.username
