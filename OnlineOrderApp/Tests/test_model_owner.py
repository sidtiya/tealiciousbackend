from django.test import TestCase
from django.contrib.auth.models import User
from OnlineOrderApp.models import Owner

import logging
logging.basicConfig(level=logging.DEBUG)

# Create your tests here.
class OwnerModelUnitTest(TestCase):

    @classmethod
    def setupTestData(cls):
        print("================= OwnerModelUnitTest =========================")

    def setUp(self):
        logging.debug("\n================= OwnerModelUnitTest Start =========================")
        logging.debug("setUp: Run once for every test method to setup cleann data")
        userInstance = User.objects.create(first_name="TestUser")
        Owner.objects.create(name="Test", user=userInstance, phone="1234567890", address="TestAddress", logo="/media/owner_logo/download_kurf8q5.png")

    def test_owner_model(self):
        owner=Owner.objects.get(id=1)
        field_label = owner._meta.get_field('name').verbose_name
        self.assertEquals(field_label,'name')
        logging.debug('OwnerName: %s ' %(owner.getName()))
        self.assertEqual(owner.getName(), "Test")
        logging.debug('OwnerAddress: %s ' %(owner.getAddress()))
        self.assertEqual(owner.getAddress(), "TestAddress")
        logging.debug('OwnerPhone: %s ' %(owner.getPhone()))
        self.assertEqual(owner.getPhone(), "1234567890")

    def tearDown(self):
        logging.debug("\n================ OwnerModelUnitTest End ===============================")
