from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from rest_framework import status
from OnlineOrderApp import apis

import json
import logging
logging.basicConfig(level=logging.DEBUG)

# Create your tests here.
class StripePaymentTests(TestCase):

    @classmethod
    def setupTestData(cls):
        print("==========================================================================")

    @staticmethod
    def createRequestPayload(price,ccy,description,stripeToken):
        payload = {}
        payload['price'] = price
        payload['ccy'] = ccy
        payload['description'] = description
        payload['stripe_token'] = stripeToken
        return payload

    def setUp(self):
        logging.debug("\n================ StripePaymentTests Start ===============================")
        logging.debug("setUp: Run once for every test method to setup clean data")
        client = Client()


    #TODO: Retrieve charge and validate charge status_code  for success and failed
    #Currently only validating stripe token but will be updated to validate Charge
    #status, however can view the charge success and fail in debug console
    def testProcessSuccessfulPayment(self):
        # get API response with a successful charge
        requestPayload = self.createRequestPayload(10,'usd','Successful Charge','tok_bypassPending')
        response = self.client.post(reverse('process-payment'), requestPayload)
        responseJson = response.json()
        logging.debug( 'Json Response for Successful Charge: %s ' %( responseJson ) )
        self.assertEqual(responseJson['status'], 'succeeded')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    #zip code error validation
    def testProcessFailedPayment(self):
        # get API response with a failed token
        requestPayload = self.createRequestPayload(10,'usd','Failed Charge','tok_avsFail')
        response = self.client.post(reverse('process-payment'), requestPayload)
        responseJson = response.json()
        logging.debug( 'Json Response for Failed Charge: %s ' %( responseJson ) )
        self.assertEqual(responseJson['status'], 'card_error: incorrect_zip')
        self.assertEqual(response.status_code, status.HTTP_402_PAYMENT_REQUIRED)

    def testJsonPayloads(self):
        data = {}
        data['key'] = 'value'
        json_data = json.dumps(data)
        self.assertTrue( apis.isJson( json_data ) )
        data = 'value'
        self.assertFalse( apis.isJson( data ) )

    def tearDown(self):
        logging.debug("\n================ StripePaymentTests End ===============================")
