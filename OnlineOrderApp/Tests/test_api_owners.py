# Django Specific Imports
from django.test import Client
from django.contrib.auth.models import User
from django.urls import reverse
from django.http import JsonResponse

# APP Imports
from OnlineOrderApp.models import Owner, Order, Drink, OrderDetails
from OnlineOrderApp.serializers import OwnerSerializer, OrderSerializer, DrinkSerializer

# JSON Import
import json
import time
from rest_framework import status
from rest_framework.test import APITestCase

# Logging Import
import logging
logging.basicConfig(level=logging.DEBUG)


# Create your tests here.
class OwnersRestAPIUnitTest(APITestCase):

    @classmethod
    def setupTestData(cls):
        print("================ OwnersRestAPIUnitTest ===============================")

    def setUp(self):
        logging.debug("\n================ OwnersRestAPIUnitTest Start ===============================")
        logging.debug("setUp: Run once for every test method to setup clean data")
        self.rest_client = Client()
        self.rest_client.login(username='sid1234', password='1234')
        userInstance = User.objects.create(first_name="TestUser")
        Owner.objects.create(name="Test", user=userInstance, phone="1234567890", address="TestAddress", logo="/media/owner_logo/download_kurf8q5.png")

    def test_get_all_owners(self):

        # get API response
        response = self.rest_client.get(reverse('get-all-owners'))
        json_response = response.json()
        response_dict = json_response["owners"][0]
        logging.debug("Values Obtained from API: %s" % response_dict)


        # get data from db
        owners = Owner.objects.all().order_by("-id")
        serializer = OwnerSerializer(owners, many=True)
        serializer_json = JsonResponse({"owners":serializer.data})
        serializer_json = serializer.data[0]
        logging.debug("Values Obtained from Database: %s" % serializer_json)

        # For each key in Data from DB, validate against
        # Data returned By Get All Owners API
        for key in serializer_json.keys():
            logging.debug("Serializer[%s]: %s" %(key, serializer_json[key]))
            if key in response_dict.keys():
                logging.debug("Response[%s]: %s" %(key, response_dict[key]))
                if key is not 'logo':
                    logging.debug(key)
                    if (response_dict[key] != serializer_json[key]):
                        logging.debug("Invalid Values of Response[%s] and Serializer[%s] Dictionaries for key[%s]" %(response_dict[key], serializer_json[key],key))
                    else:
                        logging.debug("Values are Equal for key[%s]" % key)
                else:
                    pos_a = serializer_json[key].find('media')
                    pos_b = response_dict[key].find('media')
                    logging.debug("DB Returned Stripped Value: %s" % serializer_json[key][pos_a:])
                    logging.debug("RestAPI Returned Stripped Value: %s" % response_dict[key][pos_b:])
                    if serializer_json[key][pos_a:] != response_dict[key][pos_b:]:
                        logging.debug("Invalid Values of Response[%s] and Serializer[%s] Dictionaries for key[%s]" %(response_dict[key][pos_b:], serializer_json[key][pos_a:] ,key))
                    else:
                        logging.debug("Values are Equal for key[%s]" % key)

        # Validate Status Code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        logging.debug("\n================ OwnersRestAPIUnitTest End ===============================")
