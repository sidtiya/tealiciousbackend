from django.test import TestCase
from django.contrib.auth.models import User
from OnlineOrderApp.models import Customer

import logging
logging.basicConfig(level=logging.DEBUG)

# Create your tests here.

class CustomerModelUnitTest(TestCase):

    @classmethod
    def setupTestData(cls):
        print("==========================================================================")

    def setUp(self):
        logging.debug("\n================ CustomerModelUnitTest Start ===============================")
        logging.debug("Customer Unit Test setUp: Run once for every test method to setup cleann data")
        userInstance = User.objects.create(first_name="TestFirstName", last_name="TestLastName", email="Test@Test.com")
        Customer.objects.create(user=userInstance, phone="0000000000", address="TestAddress", avatar="TestAvatar")

    def test_owner_model(self):
        customer=Customer.objects.get(id=1)
        logging.debug('CustomerName: %s ' %(customer.user.get_full_name()))
        self.assertEqual(customer.user.get_full_name(), "TestFirstName TestLastName")
        logging.debug('CustomerName: %s ' %(customer.getEmail()))
        self.assertEqual(customer.getEmail(), "Test@Test.com")
        logging.debug('CustomerPhone: %s ' %(customer.getPhone()))
        self.assertEqual(customer.getPhone(), "0000000000")
        logging.debug('CustomerAdress: %s ' %(customer.getAddress()))
        self.assertEqual(customer.getAddress(), "TestAddress")
        logging.debug('CustomerAvatar: %s ' %(customer.getAvatar()))
        self.assertEqual(customer.getAvatar(), "TestAvatar")


    def tearDown(self):
        logging.debug("\n================ CustomerModelUnitTest End ===============================")
