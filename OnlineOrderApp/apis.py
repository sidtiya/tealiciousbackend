from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.http import JsonResponse

import json
from django.db.models import Sum
#from django.db.models import prefetch_related
from oauth2_provider.models import AccessToken
from datetime import datetime

# Project Specific Imports
from OnlineOrderApp.models import Owner, Order, Drink, OrderDetails, UserRecord, Category, DrinksRecordForUser, Ingredient
from OnlineOrderApp.serializers import OwnerSerializer, OrderSerializer, DrinkSerializer, CategorySerializer, IngredientSerializer
from OnlineOrderApp.helpermethods import *
from oauth2_provider.models import AccessToken
from . import signals
from OnlineOrderApp.signals import order_placed
from django.dispatch import receiver

from cart.cart import Cart

# Our Custom Logger
logger = LoggerSingleTon('Stripe')


import stripe
from TealiciousOrdersProject.settings import STRIPE_API_KEY

stripe.api_key = STRIPE_API_KEY

###############################
### STRIPE SPECIFIC API #####
###############################
@func_detail
def payment( request ):
    try:
        #Get Stripe Token
        logging.debug( 'StripeToken: %s ' %( request.POST['stripe_token'] ) )
        stripe_token = request.POST['stripe_token']
        chargeResponse = applyCharge( 10, 'usd', 'creating a test charge', stripe_token )
        #Bind to response
        return  wrapJsonToHttp( chargeResponse )
    except Exception as e:
            logging.debug( 'Error: %s' %( e ))

@func_detail
def applyCharge( price, ccy, description, stripeToken ):
    logging.debug( 'Price: %d ' %( price ) )
    logging.debug( 'StripeToken: %s ' %( stripeToken ) )
    try:
        charge = stripe.Charge.create(
            amount = price * 100,
            currency = ccy,
            description = description,
            source = stripeToken)
        chargeJson =  jsonizedStripeCharge( 200, charge )
        logging.debug( 'Charge: %s' %( chargeJson ) )
        return chargeJson
    except stripe.error.CardError as e:
        logging.debug( 'Error: %s' %( e.json_body ))
        chargeJson =  jsonizedStripeCardError( 402, e.json_body,  price * 100 )
        logging.debug( 'CardError Json: %s' %( chargeJson ) )
        return chargeJson

def jsonizedStripeCharge( httpStatus, charge ):
    chargeJson = {}
    chargeJson['stripeId'] = charge['id']
    chargeJson['sourceId'] = charge['source']['id']
    chargeJson['description'] = charge['description']
    chargeJson['amount'] = charge['amount']
    chargeJson['timestamp'] = datetime.utcfromtimestamp( charge['created'] ).isoformat()
    chargeJson['status'] = charge['status']
    chargeJson['httpStatus'] = httpStatus
    return chargeJson

def jsonizedStripeCardError( httpStatus, cardError, amount ):
    err  = cardError.get('error', {})
    chargeJson = {}
    chargeJson['stripeId'] = err['charge']
    chargeJson['sourceId'] = err['charge']
    chargeJson['description'] = err['message']
    chargeJson['amount'] = amount
    chargeJson['timestamp'] = datetime.now().isoformat()
    chargeJson['status'] = err['type'] + ': ' + err['code']
    chargeJson['httpStatus'] = httpStatus
    return chargeJson

#Check if object is json if not return false
def isJson( object ):
    try:
        json_object = json.loads( object )
    except ValueError as e:
        logging.debug( 'StripeToken: %s ' %(False) )
        return False
    logging.debug( 'StripeToken: %s ' %(True) )
    return True

def wrapJsonToHttp( jsonPayload ):
    #ensure we only wrap json to http
    jsonDump = json.dumps( jsonPayload )
    flag = isJson( jsonDump )
    if flag:
        logging.debug( 'Json Dump: %s ' %(jsonDump) )
        return HttpResponse( jsonDump , content_type="application/json", status=jsonPayload['httpStatus'] )
    else:
        raise ValueError('Paylaod is not a valid json...')


###############################
### OWNER SPECIFIC API #####
###############################

# API To Get All Registered ShareTeaStores
@func_detail
def get_all_owners(request):
    owners = OwnerSerializer(
    Owner.objects.all().order_by("-id"),
    many=True,
    context = {"request":request}
    ).data


    return JsonResponse({"owners":owners})

# API To Get Owner by ID
@func_detail
def get_owner_by_id(request, owner_id):
    owner = OwnerSerializer(
    Owner.objects.filter(id=owner_id),
    many=True,
    context = {"request":request}
    ).data

    return JsonResponse({"owner":owner})


# API To Get All Orders
@func_detail
def get_all_orders(request):
    orders = OrderSerializer(
    Order.objects.all().order_by("-id"),
    many=True,
    context = {"request":request}
    ).data


    return JsonResponse({"orders":orders})

@func_detail
def get_all_ready_orders(request):
    orders = OrderSerializer(
    Order.objects.filter(status=Order.READY).order_by("-id"),
    many=True,
    context = {"request":request}
    ).data

    return JsonResponse({"orders":orders})

# API To Get Order by ID
@func_detail
def get_order_by_id(request, customer_id, order_id):
    """
    API to Get Order for Specific Customer using Order ID
    Queries DB and Returns JSON Blob of all Drinks
    """
    order = OrderSerializer(
    Order.objects.filter(customer_id=customer_id).filter(order_id=order_id).order_by("-id"),
    Order.objects.filter(id=order_id).values(),
    many=True,
    context = {"request":request}
    ).data


    return JsonResponse({"order":order})


@func_detail
@csrf_exempt
def get_all_products(request):
    drinks = DrinkSerializer(
    Drink.objects.all().order_by("-id"),
    many=True,
    context = {"request":request}
    ).data

    return JsonResponse({"products":drinks})


def owner_order_notification(request, last_request_time):
    notification = Order.objects.filter(owner = request.user.owner,
        created_at__gt = last_request_time).count()

    return JsonResponse({"notification": notification})


###############################
### CUSTOMER SPECIFIC API #####
###############################
#
# @func_detail
# @csrf_exempt
# def customer_get_drinks(request,owner_id):
#     """
#     API to Get All Drinks Specific To Owner
#     Queries DB and Returns JSON Blob of all Drinks
#     """
#     print (request.POST.get("access_token"))
#
#     for token in AccessToken.objects.all():
#         print(token)
#
#     access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
#         expires__gt = timezone.now())
#
#     # Get profile
#     customer = access_token.user.customer
#
#     drinks = DrinkSerializer(
#     Drink.objects.filter(owner_id=owner_id).order_by("-id"),
#     many=True,
#     context = {"request":request}
#     ).data
#
#     return JsonResponse({"drinks":drinks})


@func_detail
@csrf_exempt
def customer_get_categories(request):
    """
    API to Get Categories
    Queries DB and Returns JSON Blob of all Drinks
    """

    categories = CategorySerializer(Category.objects.all(),
        many=True,
        context = {"request":request}).data

    return JsonResponse({"categories":categories})

@func_detail
@csrf_exempt
def customer_get_topping(request):
    """
    API to Get All Drinks Specific To Owner
    Queries DB and Returns JSON Blob of all Drinks
    """
    # Get token
    # access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
    #     expires__gt = timezone.now())

    # print(access_token)
    #
    # # Get profile
    # customer = access_token.user.customer
    # owner_id = request.GET['owner_id']

    ingredients = IngredientSerializer(Ingredient.objects.all(), many=True,context = {"request":request}).data

    return JsonResponse({"toppings":ingredients})

@func_detail
@csrf_exempt
def customer_get_drinks_by_category(request):
    """
    API to Get All Drinks Based On Category
    Queries DB and Returns JSON Blob of all Drinks
    """

    theCategoryId = request.GET.get('category', '')

    if theCategoryId is not '':

        drinks = DrinkSerializer(
        Drink.objects.filter(category=Category.objects.filter(id=theCategoryId)).order_by("-id"),
        many=True,
        context = {"request":request}
        ).data
    else:
        drinks = DrinkSerializer(
        Drink.objects.all().order_by("-id"),
        many=True,
        context = {"request":request}
        ).data

    return JsonResponse({"drinks":drinks})

@func_detail
@csrf_exempt
@csrf_exempt
def customer_get_drinks(request):
    """
    API to Get All Drinks Specific To Owner
    Queries DB and Returns JSON Blob of all Drinks
    """
    # Get token
    access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
        expires__gt = timezone.now())

    print(access_token)

    # Get profile
    customer = access_token.user.customer
    owner_id = request.GET['owner_id']

    drinks = DrinkSerializer(
    Drink.objects.filter(owner_id=owner_id).order_by("-id"),
    many=True,
    context = {"request":request}
    ).data

    return JsonResponse({"drinks":drinks})





@csrf_exempt
def customer_add_order(request):
    """
    Params:
        access_token
        shareatea_owner_id
        address
        order_details (json format), example:
            [{"drink_id":1, "quantity":2}, {"drink_id":2, "quantity":3}]
        stripe_token
    return:
        {"status": "success"}
    """

    print (request.POST.get("access_token"))
    # Get token
    access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
        expires__gt = timezone.now())

    # Get profile
    customer = access_token.user.customer

    # Check if customer has orders that are not processesd or delivered
    # if Order.objects.filter(customer = customer).exclude(status = Order.DELIVERED):
    #     return JsonResponse({"status": "failed", "error": "Your Last Order must be completed"})

    if not request.POST["address"]:
        return JsonResponse({"status": "failed", "error": "Address is Required"})

    # Get Order details
    order_details = json.loads(request.POST["order_details"])
    ingredient_details = json.loads(request.POST["ingredient_details"])

    order_total = 0
    order_quantity = 0
    for drink in order_details:
        order_total += Drink.objects.get(id = drink["drink_id"]).price * drink["quantity"] + (0.96*Drink.objects.get(id = drink["drink_id"]).price*drink["quantity"])
        order_quantity += drink["quantity"]

    if len(order_details) > 0:
        # Step1 create order
        order = Order.objects.create(
            customer = customer,
            owner_id = request.POST["owner_id"],
            total = order_total,
            status = Order.MAKING,
            address = request.POST["address"],
            quantity = order_quantity
        )

        # Step 2 create order Details
        for drink in order_details:
            OrderDetails.objects.create(
            order = order,
            drink_id = drink["drink_id"],
            quantity = drink["quantity"],
            sub_total = Drink.objects.get(id = drink["drink_id"]).price*drink["quantity"] + (0.96* Drink.objects.get(id = drink["drink_id"]).price*drink["quantity"])
            )

            DrinksRecordForUser.objects.create(
            customer = customer,
            drink = Drink.objects.get(id = drink["drink_id"]),
            num_orders = drink["quantity"]
            )

            print(access_token.user.customer)

            num_orders = UserRecord.objects.all().filter(customer=access_token.user.customer).aggregate(Sum('num_orders'))

            if num_orders['num_orders__sum'] is None:
                num_orders['num_orders__sum'] = 0

            print ("\n  num_orders Quantity: " + str(num_orders['num_orders__sum']))

        # Send signal for handling status changed
            signals.order_placed.send(sender=access_token.user.customer,
               drink=Drink.objects.get(id = drink["drink_id"]),
               customer=access_token.user.customer,
               order=order,
               order_total= order_total,
               quantity= num_orders['num_orders__sum'] + order_quantity
               )


        return JsonResponse({"status":"success", "total_orders_to_date":str(num_orders['num_orders__sum'])})

@func_detail
def customer_get_latest_order(request):

    # Get token
    access_token = AccessToken.objects.get(token=request.GET.get("access_token"),
        expires__gt = timezone.now())

    # Get Customer object
    customer = access_token.user.customer

    # Get Order for customer
    order = OrderSerializer(Order.objects.filter(customer = customer).last()).data

    return JsonResponse({"orders":order})

@func_detail
def customer_get_access_token(request, customer_id):
    # Get token By Customer ID
    access_token = AccessToken.objects.get(user_id=customer_id)
    return JsonResponse({"access_token":str(access_token)})

@func_detail
@csrf_exempt
def customer_get_total_drinks(request):

    # Get token
    access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
        expires__gt = timezone.now())

    # Get profile
    num_orders =  UserRecord.objects.all().filter(user=access_token.user).aggregate(Sum('num_orders'))
    return JsonResponse({"User_Total_Orders_To_Date":str(num_orders['num_orders__sum'])})


@func_detail
@csrf_exempt
def customer_get_favorite_drinks(request):

    # Get token
    access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
        expires__gt = timezone.now())

    alldrinks = DrinksRecordForUser.objects.all().filter(customer=access_token.user.customer)
    print(alldrinks)
    #userrecords = UserRecord.objects.all().filter(user=access_token.user)
    orderedDrinks = {}
    favoriteDrink = None
    maxFav = 0
    for e in alldrinks:
        if e.drink.name in orderedDrinks:
            #print (orderedDrinks.get(e.drink.name))
            val = orderedDrinks.get(e.drink.name) + e.num_orders
            print (val)
            orderedDrinks[e.drink.name] = val
        else:
            orderedDrinks[e.drink.name] = e.num_orders

    print (json.dumps(orderedDrinks, indent=1))
    for key, value in orderedDrinks.items():
        if value > maxFav:
            maxFav = value
            favoriteDrink = key

    print(Drink.objects.get(name = favoriteDrink).price)
    return JsonResponse({"drinks:":json.dumps(orderedDrinks)})

# @func_detail
# @csrf_exempt
# def customer_get_favorite_drinks(request):
#
#     # Get token
#     access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
#         expires__gt = timezone.now())
#
#     userrecords = UserRecord.objects.all().filter(user=access_token.user)
#     for e in userrecords:
#         print(e.drink)
#     return JsonResponse({})


##############
# DRIVERS
##############

def driver_get_ready_orders(request):
    orders = OrderSerializer(
        Order.objects.filter(status = Order.READY).order_by("-id"),
        many = True
    ).data

    print(str(orders))

    return JsonResponse({"orders": orders})

@csrf_exempt
# POST
# params: access_token, order_id
def driver_pick_order(request):

    if request.method == "POST":
        # Get token

        access_token = AccessToken.objects.get(token = request.POST.get("access_token"),
            expires__gt = timezone.now())
        print (access_token)

        # Get Driver
        driver = access_token.user.driver

        # Check if driver can only pick up one order at the same time
        # if Order.objects.filter(driver = driver).exclude(status = Order.DELIVERED, driver=None):
        #     return JsonResponse({"status": "failed", "error": "You can only pick one order at the same time."})

        try:
            order = Order.objects.get(
                id = request.POST["order_id"],
                driver = None,
                status = Order.READY
            )
            order.driver = driver
            order.status = Order.ONTHEWAY
            order.picked_at = timezone.now()
            order.save()

            return JsonResponse({"status": "success"})

        except Order.DoesNotExist:
            return JsonResponse({"status": "failed", "error": "This order has been picked up by another."})

    return JsonResponse({})

# GET params: access_token
def driver_get_latest_order(request):
    # Get token
    access_token = AccessToken.objects.get(token = request.GET.get("access_token"),
        expires__gt = timezone.now())

    driver = access_token.user.driver
    order = OrderSerializer(
        Order.objects.filter(driver = driver).order_by("picked_at").last()
    ).data

    return JsonResponse({"order": order})

# POST params: access_token, order_id
@csrf_exempt
def driver_complete_order(request):
    # Get token
    access_token = AccessToken.objects.get(token = request.POST.get("access_token"),
        expires__gt = timezone.now())

    driver = access_token.user.driver

    order = Order.objects.get(id = request.POST["order_id"], driver = driver)
    order.status = Order.DELIVERED
    order.save()

    return JsonResponse({"status": "success"})

# GET params: access_token
def driver_get_revenue(request):
    access_token = AccessToken.objects.get(token = request.GET.get("access_token"),
        expires__gt = timezone.now())

    driver = access_token.user.driver

    from datetime import timedelta

    revenue = {}
    today = timezone.now()
    current_weekdays = [today + timedelta(days = i) for i in range(0 - today.weekday(), 7 - today.weekday())]

    for day in current_weekdays:
        orders = Order.objects.filter(
            driver = driver,
            status = Order.DELIVERED,
            created_at__year = day.year,
            created_at__month = day.month,
            created_at__day = day.day
        )

        revenue[day.strftime("%a")] = sum(order.total for order in orders)

    return JsonResponse({"revenue": revenue})



@csrf_exempt
def add_to_cart(request):

    # Get token
    access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
        expires__gt = timezone.now())

    # Get profile
    customer = access_token.user.customer

    # Check if customer has orders that are not processesd or delivered
    # if Order.objects.filter(customer = customer).exclude(status = Order.DELIVERED):
    #     return JsonResponse({"status": "failed", "error": "Your Last Order must be completed"})

    if not request.POST["drink_id"]:
        return JsonResponse({"status": "failed", "error": "Drink ID is Required"})


    if not request.POST["quantity"]:
        return JsonResponse({"status": "failed", "error": "Quantity is Required"})

    drink = Drink.objects.get(id = request.POST["drink_id"])
    cart = Cart(request)
    cart.add(drink, drink.price, request.POST["quantity"])

    return JsonResponse({"status":"success"})

@csrf_exempt
def remove_from_cart(request):

    # Get token
    access_token = AccessToken.objects.get(token=request.POST.get("access_token"),
        expires__gt = timezone.now())

    # Get profile
    customer = access_token.user.customer

    # Check if customer has orders that are not processesd or delivered
    # if Order.objects.filter(customer = customer).exclude(status = Order.DELIVERED):
    #     return JsonResponse({"status": "failed", "error": "Your Last Order must be completed"})

    if not request.POST["drink_id"]:
        return JsonResponse({"status": "failed", "error": "Drink ID is Required"})

    drink = Drink.objects.get(id=request.POST["drink_id"])
    cart = Cart(request)
    cart.remove(drink)

    return JsonResponse({"status":"success"})

@csrf_exempt
def get_cart(request):
    cart = Cart(request)
    drinks_list = []
    drink = {}

    # Iterate Over CART and Create Drink Object
    for item in cart:
        drink['name'] = item.product.name
        drink['short_description'] = item.product.short_description
        drink['price'] = item.product.price * item.quantity
        drink['calories'] = item.product.calories

        # Add To Drink List
        drinks_list.append(drink)
        drink = {}

    return JsonResponse({"cartList":json.dumps(drinks_list)})
