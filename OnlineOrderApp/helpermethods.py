import logging
import os.path
from django.conf import settings
from contextlib import contextmanager
from functools import wraps
from django.http import HttpResponse

#import request

logging.basicConfig(level=logging.DEBUG)

def loggerMethods(name, loggingLevel=logging.DEBUG):
    logging.basicConfig(level=loggingLevel)
    logger = logging.getLogger(name+'_'+'module')
    # Build paths inside the project like this: os.path.join(BASE_DIR, ...)
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # create debug file handler and set level to debug
    handler = logging.FileHandler(os.path.join(BASE_DIR, "all.log"),"w")
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s modulename: %(name)s %(message)s ',
                              datefmt='%Y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger

# Decorator to print func details
# Print Function name
# Print args of Function
def func_detail(func):
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        r = func(*args, **kwargs)
        logger = LoggerSingleTon(__name__)
        if isinstance(args, HttpResponse):
            logging.debug(request.url + " : " + str(request.remote_addr))
        logger.debug("Method name: " + func.__name__)
        logger.debug( [x for x in [args]])
        logger.debug( [x for x in [kwargs]])
        return r
    return func_wrapper

import os
import logging
import sys
from logging.handlers import RotatingFileHandler

try:
    # Build paths inside the project like this: os.path.join(BASE_DIR, ...)
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    #handler = logging.FileHandler(os.path.join(BASE_DIR, "all.log"),"w")
    LOG_FILENAME = os.path.join(BASE_DIR, "all.log")
    # os.path.splitext(__file__)[0] + ".log"
except:
    LOG_FILENAME = __file__ + ".log"

class Singleton(object):
    """
    Singleton interface:
    http://www.python.org/download/releases/2.2.3/descrintro/#__new__
    """
    def __new__(cls, *args, **kwds):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it
        cls.__it__ = it = object.__new__(cls)
        it.init(*args, **kwds)
        return it

    def init(self, *args, **kwds):
        pass

class LoggerManager(Singleton):
    """
    Logger Manager.
    Handles all logging files.
    """
    def init(self, loggername):
        self.logger = logging.getLogger(loggername)
        rhandler = None
        try:
            rhandler = RotatingFileHandler(
                    LOG_FILENAME,
                    mode='a',
                    maxBytes = 10 * 1024 * 1024,
                    backupCount=5
                )
        except:
            raise IOError("Couldn't create/open file \"" + \
                          LOG_FILENAME + "\". Check permissions.")

        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            fmt = '[%(asctime)s] [%(filename)s:%(lineno)d] [%(levelname)-8s] %(message)s',
            datefmt = '%F %H:%M:%S'
        )
        rhandler.setFormatter(formatter)
        self.logger.addHandler(rhandler)

    def debug(self, loggername, msg):
        self.logger = logging.getLogger(loggername)
        self.logger.debug(msg)

    def error(self, loggername, msg):
        self.logger = logging.getLogger(loggername)
        self.logger.error(msg)

    def info(self, loggername, msg):
        self.logger = logging.getLogger(loggername)
        self.logger.info(msg)

    def warning(self, loggername, msg):
        self.logger = logging.getLogger(loggername)
        self.logger.warning(msg)

class LoggerSingleTon(object):
    """
    Logger object.
    """
    def __init__(self, loggername="root"):
        self.lm = LoggerManager(loggername) # LoggerManager instance
        self.loggername = loggername # logger name

    def debug(self, msg):
        self.lm.debug(self.loggername, msg)

    def error(self, msg):
        self.lm.error(self.loggername, msg)

    def info(self, msg):
        self.lm.info(self.loggername, msg)

    def warning(self, msg):
        self.lm.warning(self.loggername, msg)
