from rest_framework import serializers

from OnlineOrderApp.models import Owner, Order, Drink, Customer, Driver, OrderDetails, Category, Ingredient


class OwnerSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()

    def get_logo(self, owner):

        request = self.context.get('request')
        print(str(owner))
        logo_url = owner.logo.url
        print(logo_url)
        print(request)
        if request is not None:
            return request.build_absolute_uri(logo_url)
        else:
            return logo_url

    class Meta:
        model = Owner
        fields = ("id", "name", "phone", "address", "logo")


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ("id", "avatar", "phone", "address")


class CategorySerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, category):
        request = self.context.get('request')
        image_url = category.image.url
        return request.build_absolute_uri(image_url)

    class Meta:
        model = Category
        fields = ("id", "name", "calories", "short_description", "image")


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ("id", "address", "total", "status", "created_at")


class DrinkSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, drink):
        request = self.context.get('request')
        image_url = drink.image.url
        return request.build_absolute_uri(image_url)

    class Meta:
        model = Drink
        fields = ("id", "name", "category", "short_description", "image", "price")


class IngredientSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, ingredient):
        request = self.context.get('request')
        image_url = ingredient.image.url
        return request.build_absolute_uri(image_url)

    class Meta:
        model = Ingredient
        fields = ("id", "name", "short_description","ingredient_type", "image", "price", "calories", "available")


# class OrderCustomerSerializer(serializers.ModelSerializer):
#     name = serializers.SerializerMethodField(source="user.get_full_name")
#
#     class Meta:
#         model = Customer
#         fields = ("id", "name", "avataar", "address", "phone")
#
# class OrderShareTeaSerializer(serializers.ModelSerializer):
#     name = serializers.SerializerMethodField(source="user.get_full_name")
#
#     class Meta:
#         model = Order
#         fields = ("id", "name", "avataar", "address", "phone")


# ORDER SERIALIZER
class OrderCustomerSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source="user.get_full_name")

    class Meta:
        model = Customer
        fields = ("id", "name", "avatar", "phone", "address")


class OrderDriverSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source="user.get_full_name")

    class Meta:
        model = Customer
        fields = ("id", "name", "avatar", "phone", "address")


class OrderOwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Owner
        fields = ("id", "name", "phone", "address")


class OrderDrinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drink
        fields = ("id", "name", "price")


class OrderDetailsSerializer(serializers.ModelSerializer):
    drink = OrderDrinkSerializer()

    class Meta:
        model = OrderDetails
        fields = ("id", "drink", "quantity", "sub_total")


class OrderSerializer(serializers.ModelSerializer):
    customer = OrderCustomerSerializer()
    driver = OrderDriverSerializer()
    owner = OrderOwnerSerializer()
    order_details = OrderDetailsSerializer(many=True)
    status = serializers.ReadOnlyField(source="get_status_display")

    class Meta:
        model = Order
        fields = ("id", "customer", "owner", "driver", "order_details", "total", "status", "address")
