
#  Django Imports
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.conf import settings

# TealiciousOrdersProject specifice Imports
from OnlineOrderApp.forms import UserForm, OwnerForm, UserFormEdit, DrinkForm, IngredientForm
from  OnlineOrderApp.helpermethods import *
from OnlineOrderApp.models import Drink, Order, Driver, Owner, Ingredient

from django.db.models import Sum, Count, Case, When

from query_logger import DatabaseQueryLoggerMixin

db_query_logger = DatabaseQueryLoggerMixin()

# Create your views here.
def home(request):
    return redirect(owner_home)

# Owner Home View
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_home(request):
    return redirect(owner_drink)

# Owner Account Form
@func_detail
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_account(request):
    user_form = UserFormEdit(instance=request.user)
    owner_form = OwnerForm(instance=request.user.owner)

    db_query_logger.start_query_logging()
    if request.method == "POST":
        user_form = UserFormEdit(request.POST, instance = request.user)
        owner_form = OwnerForm(request.POST, request.FILES, instance = request.user.owner)

        if user_form.is_valid() and owner_form.is_valid():
            user_form.save()
            owner_form.save()

    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/account.html', {
    "user_form": user_form,
    "owner_form": owner_form
    })

# Owner Drink Form
@func_detail
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_drink(request):
    db_query_logger.start_query_logging()
    drinks = Drink.objects.filter(owner = request.user.owner).order_by("id")
    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/drink.html', {"drinks": drinks})

# Owner Add New Drink Method
@func_detail
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_add_drink(request):
    form = DrinkForm()

    db_query_logger.start_query_logging()
    if request.method == "POST":
        form = DrinkForm(request.POST, request.FILES)

        if form.is_valid():
            drink = form.save(commit=False)
            drink.owner = request.user.owner
            drink.save()
            return redirect(owner_drink)

    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/add_drink.html', {
    "form": form
    })

# Owner Edit Drink Method
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_edit_drink(request, drink_id):
    form = DrinkForm(instance = Drink.objects.get(id = drink_id))
    db_query_logger.start_query_logging()
    if request.method == "POST":
        form = DrinkForm(request.POST, request.FILES, instance = Drink.objects.get(id = drink_id))

        if form.is_valid():
            drink = form.save(commit=False)
            drink.save()
            return redirect(owner_drink)
    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/edit_drink.html', {
        "form": form
    })

# Owner Dashboard Report
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_report(request):
    return render(request, 'sharetea_owner/report.html', {})


# Owner Order View
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_order(request):

    # POST Method to Set Order From `RECEIVED` To `READY` state
    db_query_logger.start_query_logging()
    if request.method == "POST":
        order = Order.objects.get(id = request.POST["id"], owner = request.user.owner)

        if order.status == Order.RECEIVED:
            order.status = Order.READY
            order.save()

    # GET Order from Database by ID for that specific owner
    orders = Order.objects.filter(owner = request.user.owner).order_by("id")
    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/order.html', {"orders": orders})

# Owner Sign-Up View
def owner_sign_up(request):
    user_form = UserForm()
    owner_form = OwnerForm()

    db_query_logger.start_query_logging()
    if request.method == "POST":
        user_form = UserForm(request.POST)
        # request Files is used to Add Owner Logo in form submit
        owner_form = OwnerForm(request.POST,request.FILES)

        if user_form.is_valid() and owner_form.is_valid():
            new_user = User.objects.create_user(**user_form.cleaned_data)
            new_owner = owner_form.save(commit=False)
            new_owner.user = new_user
            new_owner.save()

            login(request, authenticate(
                username = user_form.cleaned_data['username'],
                password = user_form.cleaned_data['password']
            ))
            db_query_logger.stop_query_logging()
            return redirect(owner_home)

        db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/sign_up.html', {
    "user_form": user_form,
    "owner_form": owner_form
    })

# Owner Report View
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_report(request):

    from datetime import datetime, timedelta

    revenue = []
    orders = []


    today = datetime.now()
    current_weekdays = [today + timedelta(days = i) for i in range(0 - today.weekday(), 7 - today.weekday())]

    for day in current_weekdays:
        delivered_orders = Order.objects.filter(
            owner = request.user.owner,
            status = Order.DELIVERED,
            created_at__year = day.year,
            created_at__month = day.month,
            created_at__day = day.day
        )
        revenue.append(sum(order.total for order in delivered_orders))
        orders.append(delivered_orders.count())


    # Top 3 Drinks
    top3_drinks = Drink.objects.filter(owner = request.user.owner)\
                     .annotate(total_order = Sum('orderdetails__quantity'))\
                     .order_by("-total_order")[:3]

    print(top3_drinks)

    drink = {
        "labels": [drink.name for drink in top3_drinks],
        "data": [drink.total_order or 0 for drink in top3_drinks]
    }

    # Top 3 Drivers
    top3_drivers = Driver.objects.annotate(
        total_order = Count(
            Case (
                When(order__owner = request.user.owner, then = 1)
            )
        )
    ).order_by("-total_order")[:3]

    driver = {
        "labels": [driver.user.get_full_name() for driver in top3_drivers],
        "data": [driver.total_order for driver in top3_drivers]
    }
    print(drink)

    return render(request, 'sharetea_owner/report.html', {
        "revenue": revenue,
        "orders": orders,
        "drink": drink,
        "driver": driver
    })



# Owner Ingredient Form
@func_detail
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_ingredient(request):
    db_query_logger.start_query_logging()
    ingredients = Ingredient.objects.all().order_by("id")
    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/ingredient.html', {"ingredients": ingredients})

# Owner Add New Drink Method
@func_detail
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_add_ingredient(request):
    form = IngredientForm()

    db_query_logger.start_query_logging()
    if request.method == "POST":
        form = IngredientForm(request.POST, request.FILES)

        if form.is_valid():
            ingredient = form.save(commit=False)
            ingredient.owner = request.user.owner
            ingredient.save()
            return redirect(owner_ingredient)

    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/add_ingredient.html', {
    "form": form
    })

# Owner Edit Drink Method
@login_required(login_url='/sharetea_owner/sign-in/')
def owner_edit_ingredient(request, ingredient_id):
    form = IngredientForm(instance = Ingredient.objects.get(id = ingredient_id))
    db_query_logger.start_query_logging()
    if request.method == "POST":
        form = IngredientForm(request.POST, request.FILES, instance = Ingredient.objects.get(id = ingredient_id))

        if form.is_valid():
            ingredient = form.save(commit=False)
            ingredient.save()
            return redirect(owner_ingredient)
    db_query_logger.stop_query_logging()
    return render(request, 'sharetea_owner/edit_ingredient.html', {
        "form": form
    })
