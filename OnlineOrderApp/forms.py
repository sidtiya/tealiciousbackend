from django import forms

from django.contrib.auth.models import User
from OnlineOrderApp.models import Owner, Drink, Ingredient

class UserForm(forms.ModelForm):
    email = forms.CharField(max_length=100, required=True)
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ("username", "password", "first_name", "last_name", "email")


class UserFormEdit(forms.ModelForm):
    email = forms.CharField(max_length=100, required=True)
    #password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email")

# class RestaurantForm(forms.ModelForm):
class OwnerForm(forms.ModelForm):
    class Meta:
        model = Owner
        fields = ("name", "phone", "address", "zipcode", "logo")

class DrinkForm(forms.ModelForm):
    class Meta:
        model = Drink
        exclude = ("owner",)

class IngredientForm(forms.ModelForm):
    class Meta:
        model = Ingredient
        exclude = ("drink",)
