import string

from django.contrib.auth.models import User
from django.utils.crypto import get_random_string

from celery import *

import django
from django.conf import settings
from django.core.mail import send_mail


# Sample of a Shared Task Decorator
@shared_task
def create_random_user_accounts(total):
    for i in range(total):
        username = 'user_{}'.format(get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username)
        password = get_random_string(50)
        User.objects.create_user(username=username, email=email, password=password)
    return '{} random users created with success!'.format(total)


# Sample of adding method using Task Decorator
@task
def add(x, y):
    return x + y

# All long running methods should be defined with Task Decorator
@task
def send_email(email_text,email_user = []):
    send_mail('Subject here', 'Here is the message.', settings.EMAIL_HOST_USER,
         ['siddimore@gmail.com'], fail_silently=False)
