from __future__ import unicode_literals

from django.apps import AppConfig


class OnlineorderappConfig(AppConfig):
    name = 'OnlineOrderApp'

    def ready(self):
        # signals are imported, so that they are defined and can be used
        print("Inside Ready Method")
        import OnlineOrderApp.signals
        import OnlineOrderApp.receivers
