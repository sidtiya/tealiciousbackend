from django.contrib import admin

# Register your models here.
from OnlineOrderApp.models import Owner, Customer, Driver, Drink, Order, OrderDetails, Ingredient, Category, UserRecord, DrinkRecord, DrinksRecordForUser

admin.site.register(Owner)
admin.site.register(Customer)
admin.site.register(Driver)
admin.site.register(Drink)
admin.site.register(Order)
admin.site.register(OrderDetails)
admin.site.register(Ingredient)
admin.site.register(Category)
admin.site.register(DrinkRecord)
admin.site.register(UserRecord)
admin.site.register(DrinksRecordForUser)
