
import OnlineOrderApp.models
from django.dispatch import receiver
from OnlineOrderApp.signals import order_placed
from OnlineOrderApp.models import DrinkRecord, UserRecord

# DrinkRecord = get_classes(
#     'models', ['DrinkRecord'])


def _update_counter(model, field_name, filter_kwargs, increment=1):
    """
    Efficiently updates a counter field by a given increment. Uses Django's
    update() call to fetch and update in one query.
    TODO: This has a race condition, we should use UPSERT here
    :param model: The model class of the recording model
    :param field_name: The name of the field to update
    :param filter_kwargs: Parameters to the ORM's filter() function to get the
                          correct instance
    """
    try:
        record = model.objects.filter(**filter_kwargs)
        affected = record.update(**{field_name: F(field_name) + increment})
        if not affected:
            filter_kwargs[field_name] = increment
            model.objects.create(**filter_kwargs)
    except IntegrityError:      # pragma: no cover
        # get_or_create has a race condition (we should use upsert in supported)
        # databases. For now just ignore these errors
        logger.error(
            "IntegrityError when updating analytics counter for %s", model)


def _record_drinks_in_order(order,  order_total, order_quantity):
    # surely there's a way to do this without causing a query for each line?
    #for line in order.lines.all():
    _update_counter(
        DrinkRecord)

def _record_user_order(customer, drink, order, order_total, quantity):
    try:
        # Get Specific User To Update Number Of Orders
        #record = UserRecord.objects.filter(user=user)
        # Update Record
        # affected = record.update(
        #     num_orders = quantity,
        #     total_spent = order_total,
        #     drink = drink,
        #     date_last_order=order.created_at)
        # print("status of affected %s" %affected)
        # if not affected:
        #     print("Record User Order %s" % quantity)
        UserRecord.objects.create(
            customer = customer,
            num_orders = quantity,
            total_spent = order_total,
            drink = drink,
            date_last_order = order.created_at)
    except Exception as ex:      # pragma: no cover
        print(
            "IntegrityError in analytics when recording a user order." + str(ex))


@receiver(order_placed)
def receive_order_placed(sender, drink, customer, order, order_total, quantity, **kwargs):
    print("Received Signal")
    if kwargs.get('raw', False):
        return
    #_record_drinks_in_order(drink, order_total, quantity)
    if customer:
        _record_user_order(customer, drink, order, order_total, quantity)
